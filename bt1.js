document.getElementById("ket_qua").addEventListener("click", function () {
    var tenNguoiDung = document.getElementById("ho_ten").value;
    var tongThuNhap = document.getElementById("tong_thu_nhap").value;
    var soNguoi = document.getElementById("nguoi_phu_thuoc").value;
    var chiuThue = tongThuNhap - 4 - soNguoi * 1.6;
    var tienThue = null;
    if (chiuThue <= 60) {
      tienThue = (chiuThue * 5) / 100;
    } else if (chiuThue <= 120) {
      tienThue = (60 * 5) / 100 + ((chiuThue - 60) * 10) / 100;
    } else if (chiuThue <= 210) {
      tienThue =
        (60 * 5) / 100 + (60 * 10) / 100 + ((chiuThue - 120) * 15) / 100;
    } else if (chiuThue <= 384) {
      tienThue =
        (60 * 5) / 100 +
        (60 * 10) / 100 +
        (90 * 15) / 100 +
        ((chiuThue - 210) * 20) / 100;
    } else if (chiuThue <= 624) {
      tienThue =
        (60 * 5) / 100 +
        (60 * 10) / 100 +
        (90 * 15) / 100 +
        (174 * 20) / 100 +
        ((chiuThue - 384) * 25) / 100;
    } else if (chiuThue <= 960) {
      tienThue =
        (60 * 5) / 100 +
        (60 * 10) / 100 +
        (90 * 15) / 100 +
        (174 * 20) / 100 +
        ((624 - 384) * 20) / 100 +
        ((chiuThue - 624) * 30) / 100;
    } else {
      tienThue =
        (60 * 5) / 100 +
        (60 * 10) / 100 +
        (90 * 15) / 100 +
        (174 * 20) / 100 +
        ((624 - 384) * 25) / 100 +
        ((960 - 612) * 30) / 100 +
        ((chiuThue - 960) * 35) / 100;
    }
    // console.log({ tongThuNhap, soNguoiPhuThuoc, chiuThue, tienThue });
    document.getElementById("hotennguoidung").innerHTML= `Họ tên: ${tenNguoiDung}`;
    document.getElementById(
      "nguoiphuthuoc"
    ).innerHTML = `Số người phụ thuộc: ${soNguoi} người`;
    document.getElementById(
      "sotienphainop"
    ).innerHTML = `Số tiền phải nộp ${tienThue}  VND`;
  });
  